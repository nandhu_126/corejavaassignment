package CoreJava;
class MyException extends Exception
{
    public MyException(String s)
    {
        super(s);
    }
}
 
class Percentage {
int empNo;
String ename;
double salary;
String departmentName;

	public Percentage() {
	super();
}

	public double getSalary() {
	return salary;
}

public void setSalary(double salary) {
	this.salary = salary;
}
public void salIncrease(int percentage) throws MyException{
	double salaryfixed=24000;
	double salaryrequired=this.getSalary();
	int percent=(int)((salaryrequired*100)/salaryfixed);
	//System.out.println(percent);
	if(percent>percentage){
		throw new MyException("Salary Overflow");
	}
}
 }
 public class Salary{
	public static void main(String[] args) {
		Percentage s=new Percentage();
		s.setSalary(48000);
        try{
        	s.salIncrease(100);
        }catch(MyException e){
        	System.out.println(e.getMessage());
        }
	}

}
