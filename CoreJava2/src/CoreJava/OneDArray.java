package CoreJava;

import java.util.Arrays;
import java.util.Scanner;

class Employee {
	int empNo;
	String eName;
	double salary;
	String departmentName;
	String location;

	public Employee(int empNo, String eName, double salary, String departmentName, String location) {
		super();
		this.empNo = empNo;
		this.eName = eName;
		this.salary = salary;
		this.departmentName = departmentName;
		this.location = location;
	}

	@Override
	public String toString() {
		return "Employee [empNo=" + empNo + ", eName=" + eName + ", salary=" + salary + ", departmentName="
				+ departmentName + ", location=" + location + "]";
	}

	public double getSalary() {
		return salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

}

public class OneDArray {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		Employee emp[] = new Employee[10];
		for (int i = 0; i < 2; i++) {
			int empno = in.nextInt();
			String ename = in.next();
			double salary = in.nextDouble();
			String dname = in.next();
			String location = in.next();
			emp[i] = new Employee(empno, ename, salary, dname, location);

		}
		int a[] = new int[100];
		for (int i = 0; i <10; i++) {
			a[i] = (int) emp[i].getSalary();
		}
		System.out.println("Employee salary in ascending order:");
		for (int i = 0; i <10; i++) {
			for(int j=i+1;j<10;j++){
				if(a[i]>a[j]){
					int temp=a[i];
					a[i]=a[j];
					a[j]=temp;
				}
			}
			System.out.println(a[i]);
		}
		System.out.println("List of Employees:");
		for (int i = 0; i < 10; i++) {
			System.out.println(emp[i].toString());

		}
	}

}
